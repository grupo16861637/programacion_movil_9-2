package com.example.helloworld;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MenuActivity extends AppCompatActivity {

    private CardView crvPrimer, crvImc, crvCambio, crvConversion, crvCotizacion, crvSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        iniciarComponentes();
        crvPrimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, IMCActivity.class);
                startActivity(intent);
            }
        });

        crvConversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ConverGradosActivity.class);
                startActivity(intent);
            }
        });

        crvCambio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ConverMonedaActivity.class);
                startActivity(intent);
            }
        });

        crvCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, cotizacionIngresoActivity.class);
                startActivity(intent);
            }
        });

        crvSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, SpinnerActivity.class);
                startActivity(intent);
            }
        });

        EdgeToEdge.enable(this);
        ViewGroup rootView = findViewById(android.R.id.content);
        ViewCompat.setOnApplyWindowInsetsListener(rootView, (v, insets) -> {
            v.setPadding(
                    insets.getSystemWindowInsetLeft(),
                    insets.getSystemWindowInsetTop(),
                    insets.getSystemWindowInsetRight(),
                    insets.getSystemWindowInsetBottom()
            );
            return insets.consumeSystemWindowInsets();
        });
    }

    private void iniciarComponentes() {
        crvPrimer = findViewById(R.id.crvHola);
        crvImc = findViewById(R.id.crvImc);
        crvCambio = findViewById(R.id.crvCambio);
        crvConversion = findViewById(R.id.crvConversion);
        crvCotizacion = findViewById(R.id.crvCotizacion);
        crvSpinner = findViewById(R.id.crvSpinner);
    }
}
