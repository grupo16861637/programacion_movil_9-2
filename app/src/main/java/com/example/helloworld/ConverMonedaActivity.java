package com.example.helloworld;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ConverMonedaActivity extends AppCompatActivity {

    private EditText etCantidad;
    private Spinner spinnerConversion;
    private TextView tvResultado;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conver_moneda);

        // Inicializar vistas
        etCantidad = findViewById(R.id.etCantidad);
        spinnerConversion = findViewById(R.id.spinnerConversion);
        tvResultado = findViewById(R.id.tvResultado);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        // Configurar Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.conversiones, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerConversion.setAdapter(adapter);

        // Configurar OnClickListener para los botones
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularConversion();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void calcularConversion() {
        String cantidadStr = etCantidad.getText().toString();
        if (cantidadStr.isEmpty()) {
            Toast.makeText(this, "Introduce una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidad = Double.parseDouble(cantidadStr);
        int posicion = spinnerConversion.getSelectedItemPosition();
        double resultado = 0;

        switch (posicion) {
            case 0:
                // Pesos Mexicanos a Dólares Americanos
                resultado = cantidad * 0.05981;
                break;
            case 1:
                // Pesos Mexicanos a Euros
                resultado = cantidad * 0.05532;
                break;
            case 2:
                // Pesos Mexicanos a Dólar Canadiense
                resultado = cantidad * 0.08215;
                break;
            case 3:
                // Pesos Mexicanos a Libra Esterlina
                resultado = cantidad * 0.04715;
                break;
        }

        tvResultado.setText(String.format("Resultado: %.2f", resultado));
    }

    private void limpiarCampos() {
        etCantidad.setText("");
        spinnerConversion.setSelection(0);
        tvResultado.setText("Resultado");
    }
}
