package com.example.helloworld;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class IMCActivity extends AppCompatActivity {

    private EditText editTextWeight;
    private EditText editTextHeight;
    private Button buttonCalculate;
    private Button buttonLimpiar;
    private Button buttonCerrar;
    private TextView textViewIMC;
    private TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imcactivity);

        editTextWeight = findViewById(R.id.editTextWeight);
        editTextHeight = findViewById(R.id.editTextHeight);
        buttonCalculate = findViewById(R.id.buttonCalculate);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);
        buttonCerrar = findViewById(R.id.buttonCerrar);
        textViewIMC = findViewById(R.id.textViewIMC);
        textViewResult = findViewById(R.id.textViewResult);

        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateIMC();
            }
        });

        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFields();
            }
        });

        buttonCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calculateIMC() {
        String weightStr = editTextWeight.getText().toString();
        String heightStr = editTextHeight.getText().toString();

        if (TextUtils.isEmpty(weightStr) || TextUtils.isEmpty(heightStr)) {
            textViewResult.setText("Por favor, introduce tu peso y altura.");
            return;
        }

        float weight = Float.parseFloat(weightStr);
        float height = Float.parseFloat(heightStr);

        if (height == 0) {
            textViewResult.setText("La altura no puede ser 0.");
            return;
        }

        float bmi = weight / (height * height);

        textViewIMC.setText(String.format("%.2f", bmi));
        textViewResult.setText("Tu IMC es:");
    }

    private void clearFields() {
        editTextWeight.setText("");
        editTextHeight.setText("");
        textViewIMC.setText("0");
        textViewResult.setText("Tu IMC es:");
    }
}
